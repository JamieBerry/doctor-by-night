﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.Audio;

public class TutPauseMenuControl : MonoBehaviour {

	public GameObject PauseMenu;
	public GameObject ControlPage;

	public GameObject PausedBackground;

	public GameObject MuteAudio;
	public GameObject PlayAudio;

	void Start () {
		PauseMenu.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			PauseGame ();
		}
	}

	public void ResumeGame(){
		Time.timeScale = 1;
		PauseMenu.SetActive (false);
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		GameObject.Find ("FPSController").GetComponentInChildren<FirstPersonController> ().enabled = true;
	}

	public void ShowControls(){
		PausedBackground.SetActive (false);
		ControlPage.SetActive (true);
	}

	public void MuteSounds(){
		AudioListener.pause = true;
		PlayAudio.SetActive (true);
		MuteAudio.SetActive (false);
	}

	public void PlaySounds(){
		AudioListener.pause = false;
		PlayAudio.SetActive (false);
		MuteAudio.SetActive (true);
	}

	public void BackButton(){
		ControlPage.SetActive (false);
		PausedBackground.SetActive (true);
	}

	public void GOTOMenu(){
		Time.timeScale = 1;
		GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty = 0;
		SceneManager.LoadScene (0);
	}

	public void PauseGame(){
		PauseMenu.SetActive (true);
		Time.timeScale = 0;
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;
		GameObject.Find ("FPSController").GetComponentInChildren<FirstPersonController> ().enabled = false;
	}
}
