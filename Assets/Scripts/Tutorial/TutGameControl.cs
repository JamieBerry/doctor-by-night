﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutGameControl : MonoBehaviour {

	public GameObject PatientObject;
	public GameObject PatientSpawn;

	public int PatientKilled;
	public int PatientSaved;

	public Text PatientSavedText;

	public AudioSource SavedSound;

	void Start () {
		PatientSaved = 0;
		PatientKilled = 0;
		RespawnPatient ();
	}
	
	void Update () {
		PatientSavedText.text = "Patients Saved: " + PatientSaved.ToString();
		if (PatientKilled == 3) {
			SceneManager.LoadScene (2);
		}
	}

	public void RespawnPatient(){
		SavedSound.PlayOneShot (SavedSound.clip);
		Instantiate (PatientObject, PatientSpawn.transform.position, PatientSpawn.transform.rotation);
	}


}
