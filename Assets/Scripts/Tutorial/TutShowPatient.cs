﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutShowPatient : MonoBehaviour {

	public GameObject PatientText;
	public GameObject Patient;

	public Text PatientInfoText;
	public Text PatientLifeProgress;
	public Text PatientTemperature;

	public int PatientNo;

	public float SkinColour;
	public float Temperature;
	public Slider TemperatureSlide;
	public float HeartRate;
	public float LifeProgress;
	public Slider LifeSlide;

	public int Efficiency;						//0 = 0% Efficient	-	1 = 50% Efficient	-	2 = 100% Efficient			//0 = 50% Life Progress Raise	-	1 = 75% Life Progress Raise	-	2 = Regular Life Progress Raise

	public GameObject Arm;

	public bool GoodHeartRate;

	public GameObject HealthyBreathing;
	public GameObject HeartBeat;
	public GameObject Panting;
	public GameObject Coughing;

	void Start(){
		PatientText.SetActive (false);
		Patient = GameObject.Find ("TutPatient1(Clone)");

//		Patient.SetActive (false);

		HeartRate = 80;								//MAYBE SET WHEN THEY APPLY THE POTION
		SkinColour = 50;
		Temperature = 37;
		LifeProgress = 96;
	}

	void Update ()
	{
		Patient = GameObject.Find ("TutPatient1(Clone)");

		TemperatureSlide.value = Temperature;
		LifeSlide.value = LifeProgress;

		PatientInfoText.text = "BPM: " + Mathf.Round (HeartRate);
		PatientLifeProgress.text = Mathf.Round (LifeProgress) + "%";
		PatientTemperature.text = Mathf.Round (Temperature) + "°C";

		if (GoodHeartRate) {
			{
				HealthyBreathing.SetActive (true);
				Coughing.SetActive (false);
				LifeBar ();
			}
		}
		
		if (LifeProgress >= 100) {
			LifeProgress = 0;
			Destroy (Patient);
			GameObject.Find ("GameController").GetComponentInChildren<TutorialControl> ().ShowWell ();
//			NewPatient ();
		}

		if (!GoodHeartRate) {
			HealthyBreathing.SetActive (false);
		}


		//HEART RATE
		if (HeartRate <= 60) {
			PatientInfoText.text = "DEAD";
			//HR TOO LOW TEXT
		}

		if (HeartRate >= 60 && HeartRate <= 70) {										// if above 60 but below 70 play heartbeat
			HeartBeat.SetActive (true);
		}																			

		if (HeartRate >= 60 && HeartRate <= 75) {
			HeartBeat.SetActive (false);												// stops heartbeat sound
			Invoke ("LowerHeartRate", 0);
			if (LifeProgress > 0) {
				Invoke ("LowerLifeProgress", 0);
			}
		}
		if (HeartRate >= 75 && HeartRate <= 99.5f) {
//			Invoke ("LowerHeartRateFast", 0);
		}
		if (HeartRate >= 95 && HeartRate <= 105) {
			GoodHeartRate = true;
		} else {
			GoodHeartRate = false;
		}
		if (HeartRate >= 99.5f && HeartRate <= 145) {
//			Invoke ("RaiseHeartRateFast", 0);
		}
		if (HeartRate >= 145 && HeartRate <= 160) {
			HeartBeat.SetActive (false);												// stops heartbeat sound
			Invoke ("RaiseHeartRate", 0);
			if (LifeProgress > 0) {
				Invoke ("LowerLifeProgress", 0);
			}
		}

		if (HeartRate >= 150 && HeartRate <= 160) {										// if below 160 but above 150 play heartbeat
			HeartBeat.SetActive (true);
		}																			

		if (HeartRate >= 70 && HeartRate <= 150) {
			HeartBeat.SetActive (false);
		}

		if (HeartRate >= 160) {
			PatientInfoText.text = "DEAD";
			//HR TOO HIGH TEXT
		}

		//TEMPERATURE
		if (Temperature < 24) {
			PatientTemperature.text = "DEAD";
			//TEMP TOO LOW TEXT
		}
		if (Temperature >= 24 && Temperature <= 30) {
			Efficiency = 0;
			Panting.SetActive (true);
		}
		if (Temperature >= 31 && Temperature <= 35) {
			Efficiency = 1;
		}
		if (Temperature >= 36 && Temperature <= 37) {
			Efficiency = 2;
		}
		if (Temperature >= 38 && Temperature <= 40) {
			Efficiency = 1;
		}
		if (Temperature >= 41 && Temperature <= 44) {
			Efficiency = 0;
			Panting.SetActive (true);
		}
		if (Temperature > 44) {
			PatientTemperature.text = "DEAD";
			//TEMP TOO HIGH TEXT
		}
		if (Temperature >= 30 && Temperature <= 41) {
			Panting.SetActive (false);
		}



		if (Temperature >= 24 && Temperature < 36.5f) {
//			Temperature -= 0.01f * Time.deltaTime;				//-1 every 10 seconds

		}
		if (Temperature > 36.5f && Temperature <= 44) {
//			Temperature += 0.01f * Time.deltaTime;				//+1 every 10 seconds
		}
	}


	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player") {
			PatientText.SetActive (true);
		}
	}
	void OnTriggerExit(Collider col){
		if (col.gameObject.tag == "Player") {
			PatientText.SetActive (false);
		}
	}

	void LowerLifeProgress(){
		LifeProgress -= 0.0001f * Time.deltaTime;
	}
	void RaiseLifeProgress100(){
		LifeProgress += 6 * Time.deltaTime;
	}
	void RaiseLifeProgress75(){
		LifeProgress += 4 * Time.deltaTime;
	}
	void RaiseLifeProgress50(){
		LifeProgress += 2 * Time.deltaTime;
	}

	void LowerHeartRate(){
		HeartRate -= 0.00001f * Time.deltaTime;
		Coughing.SetActive (true);
	}

	void LowerHeartRateFast(){
		HeartRate -= 0.0001f * Time.deltaTime;
		Coughing.SetActive (false);
	}

	void RaiseHeartRate(){
		HeartRate += 0.05f * Time.deltaTime;
		Coughing.SetActive (true);
	}

	void RaiseHeartRateFast(){
		HeartRate += 0.1f * Time.deltaTime;
		Coughing.SetActive (false);
	}

	public void NewPatient(){
		GameObject.Find("GameController").GetComponentInChildren<TutGameControl>().PatientSaved += 1;
		GameObject.Find ("GameController").GetComponentInChildren<TutGameControl> ().RespawnPatient ();
	}

	public void KillPatient(){
		LifeProgress = 0;
		Destroy (Patient);
		GameObject.Find("GameController").GetComponentInChildren<TutGameControl>().PatientSaved -= 1;
		GameObject.Find ("GameController").GetComponentInChildren<TutGameControl> ().RespawnPatient ();
	}

	public void LifeBar(){
		if (Efficiency == 2) {
			RaiseLifeProgress100 ();
		}
		if (Efficiency == 1) {
			RaiseLifeProgress75 ();
		}
		if (Efficiency == 0) {
			RaiseLifeProgress50 ();
		}
	}
}

