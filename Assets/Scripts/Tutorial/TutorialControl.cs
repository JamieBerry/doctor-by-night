﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialControl : MonoBehaviour {

//	public GameObject ItemsText;

	public GameObject Welcome,Tool,Notebook,ChangePage,CloseBook,Water,Drop,Ingr,Boil,Make,Give,Well,Easy;
	public AudioSource NextHintSound;
	public bool BookHintsFinished;

	void Start(){
		BookHintsFinished = false;
		Welcome.SetActive (true);
		NextHintSound.PlayOneShot (NextHintSound.clip);
		Invoke ("ShowTool", 10);
	}

	public void ShowTool(){
		GameObject.Find ("FPSController").GetComponentInChildren<TutPlayerInteract> ().EnabledBook = true;
		Welcome.SetActive (false);
		Tool.SetActive (true);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	}
		
	public void StartShowingNoteBookHints(){	//WILL BE CALLED AFTER NOTEBOOK IS OPENED
		Tool.SetActive(false);
		Invoke("ShowNotebook",2);

	}

	public void ShowNotebook(){
		Notebook.SetActive (true);
		Invoke ("ShowChangePage", 10);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	}

	public void ShowChangePage(){
		Notebook.SetActive (false);
		ChangePage.SetActive (true);
		Invoke ("ShowCloseBook", 10);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	}

	public void ShowCloseBook(){
		ChangePage.SetActive (false);
		CloseBook.SetActive (true);
		Invoke ("ShowIngr", 10);
		NextHintSound.PlayOneShot (NextHintSound.clip);

		}

	public void ShowIngr(){						//WILL BE CALLED WHEN THE BOOK IS CLOSED AFTER OTHER HINTS
			CloseBook.SetActive (false);
			Water.SetActive (true);

			GameObject.Find ("FPSController").GetComponentInChildren<TutPlayerInteract> ().Enabled = true;
			GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnLiquid (0);
			GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (0);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	
		Invoke ("ShowDrop", 7);

		}

	public void ShowDrop(){						//WILL BE CALLED WHEN ITEM IS PICKED UP
		Water.SetActive (false);
		Drop.SetActive (true);
		Invoke("InIngr",7);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	}

	public void InIngr(){						//WILL BE CALLED WHEN ITEM IS ADDED TO MIX (MAYBE PICKUPABLE = FALSE && NOLIQUID = TRUE)
		Drop.SetActive (false);
		Ingr.SetActive (true);
		Invoke ("ShowBoil", 7);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	}

	public void ShowBoil(){						//WILL BE CALLED ONCE 1 INGREDIENT IS ADDED
		Ingr.SetActive (false);
		Boil.SetActive (true);
		Invoke ("ShowMake", 7);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	}

	public void ShowMake(){						//WILL BE CALLED AFTER 7 SECONDS AFTER BOILING 
		Boil.SetActive (false);
		Make.SetActive (true);
		Invoke ("ShowGive", 7);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	}

	public void ShowGive(){
		GameObject.Find ("TutPatient1(Clone)").GetComponentInChildren<TutShowPatient>().HeartRate = 93;
		GameObject.Find ("TutPatient1(Clone)").GetComponentInChildren<TutShowPatient>().Temperature = 37;

		Make.SetActive (false);
		Give.SetActive (true);
		NextHintSound.PlayOneShot (NextHintSound.clip);
	}

	public void ShowWell(){
			Give.SetActive (false);
			Well.SetActive (true);
			Invoke ("ShowEasy", 7);
			NextHintSound.PlayOneShot (NextHintSound.clip);
		}

	public void ShowEasy(){						//CALLED WHEN PATIENT HEALED FULLY
		Well.SetActive (false);
		Easy.SetActive (true);
		NextHintSound.PlayOneShot (NextHintSound.clip);

	}
}
