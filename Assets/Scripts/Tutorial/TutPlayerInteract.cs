﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class TutPlayerInteract : MonoBehaviour {
	
	public GameObject Notebook;

	public int CurrentPage;				//0 = plants, 1 = minerals, 2 = insects, 3 = animals

	public GameObject PlantPage;
	public GameObject MineralPage;

	public Transform HoldingAreaTransform;
	public Vector3 HoldingArea;

	public LayerMask Pickups;
	private GameObject[] PickupObject;

	public bool Pickupable;

	public Text ItemText;

	public bool Enabled;

	public bool EnabledBook;


	public bool NotebookOpen;

	public bool ShownHint1;					//used to only show hint when opened book once

	void Start () {
		Pickupable = false;
		HoldingAreaTransform = GameObject.Find ("HoldingHand").transform;										//Sets holding area transform
		HoldingArea = GameObject.Find ("HoldingHand").transform.position;										//SetsHolding area vector3
		PickupObject = GameObject.FindGameObjectsWithTag ("Pickup");											//Sets all the pickups in an array
		Notebook.SetActive (false);
		Cursor.lockState = CursorLockMode.Locked;
		CurrentPage = 0;
		Enabled = false;
		NotebookOpen = false;
		ShownHint1 = false;
	}




	void Update () {

		if (NotebookOpen == false) {
			Notebook.SetActive (false);
			ItemText.enabled = true;
		}

		if (EnabledBook == true) {
			if (Input.GetKey (KeyCode.Tab)) {
				NotebookOpen = true;
				if (NotebookOpen == true) {

					if (ShownHint1 == false) {
						GameObject.Find ("GameController").GetComponentInChildren<TutorialControl> ().StartShowingNoteBookHints ();
						ShownHint1 = true;
					}
				
					Notebook.SetActive (true);
					ItemText.enabled = false;

					if (Input.GetButtonDown ("Fire2")) {
						CurrentPage += 1;
					}
					if (Input.GetButtonDown ("Fire1")) {
						CurrentPage -= 1;
					}

					if (CurrentPage < 0) {
						CurrentPage = 0;
					}
					if (CurrentPage > 1) {
						CurrentPage = 1;
					}

					if (CurrentPage == 0) {
						PlantPage.SetActive (true);
						MineralPage.SetActive (false);
					}

					if (CurrentPage == 1) {
						PlantPage.SetActive (false);
						MineralPage.SetActive (true);
					}
				} 
			} else {
				NotebookOpen = false;
			}
		}

		HoldingAreaTransform = GameObject.Find ("HoldingHand").transform;										//Updates holding area tranform
		HoldingArea = GameObject.Find ("HoldingHand").transform.position;										//Updates holding area vector3

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);											//Creates ray from mouse
		RaycastHit hit;
		if (Enabled == true) {
			if (Physics.Raycast (ray, out hit, 2, Pickups)) {														//Projects ray 2 distance and looks for pickup layer only
				Debug.DrawLine (ray.origin, hit.point);																//Draws line to help me know what is hit and when
				GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().color = Color.blue;					//Turns crosshair blue when over a pickupable item
				Pickupable = true;
				ItemText.text = hit.collider.GetComponentInChildren<NameScript> ().IngridientName;
			} else {
				Pickupable = false;
				GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().color = Color.green;					//Turns crosshair green when not over a pickupable item
				ItemText.text = "";

				foreach (GameObject obj in PickupObject) {
					if (obj != null) {
						obj.GetComponent<Gravity> ().GravityON ();
					}
				}
			}

			if (Pickupable == true && Input.GetButtonDown ("Fire1")) {
				hit.transform.position = HoldingArea;
				hit.rigidbody.useGravity = false;
				hit.rigidbody.isKinematic = true;
				hit.transform.SetParent (HoldingAreaTransform);
				GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().enabled = false;

			}
			if (Pickupable == true && Input.GetButtonDown ("Fire2")) {
				hit.rigidbody.useGravity = true;
				hit.rigidbody.isKinematic = false;
				GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().enabled = true;
			}

			if (Pickupable == false) {
				HoldingAreaTransform.DetachChildren ();
				GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().enabled = true;
			}
		}
	}
}
