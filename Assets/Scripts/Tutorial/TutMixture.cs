﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutMixture : MonoBehaviour {
	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Dead") {

			Destroy (gameObject);

			if (this.GetComponent<Ingredient> ().Temperature == 0) {
				col.gameObject.GetComponentInParent<TutShowPatient> ().Temperature -= 2;
			}
			if (this.GetComponent<Ingredient> ().Temperature == 1) {
				col.gameObject.GetComponentInParent<TutShowPatient> ().Temperature -= 0;
			}

			if (this.GetComponent<Ingredient> ().Temperature == 2) {
				col.gameObject.GetComponentInParent<TutShowPatient> ().Temperature += 2;
			}
																//HEART RATE
					//100% Effective
			if (col.gameObject.GetComponentInParent<TutShowPatient> ().Efficiency == 2) {
				if (this.GetComponent<Ingredient> ().HeartRate == -1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (7, 10));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (17, 20));

				}
				if (this.GetComponent<Ingredient> ().HeartRate == -3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (27, 30));

				}
				if (this.GetComponent<Ingredient> ().HeartRate <= -4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= 100;
				}

				if (this.GetComponent<Ingredient> ().HeartRate == 0) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += 0;
				}

				if (this.GetComponent<Ingredient> ().HeartRate == 1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (7, 10));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (17, 20));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (27, 30));
				}
				if (this.GetComponent<Ingredient> ().HeartRate >= 4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += 100;
				}
			}
					//50% Effective
			if (col.gameObject.GetComponentInParent<TutShowPatient> ().Efficiency == 1) {
				if (this.GetComponent<Ingredient> ().HeartRate == -1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (5, 7));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (14, 17));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (23, 27));
				}
				if (this.GetComponent<Ingredient> ().HeartRate <= -4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= 100;
				}

				if (this.GetComponent<Ingredient> ().HeartRate == 0) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += 0;
				}
	
				if (this.GetComponent<Ingredient> ().HeartRate == 1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (5, 7));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (14, 17));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (23, 27));
				}
				if (this.GetComponent<Ingredient> ().HeartRate >= 4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += 100;
				}
			}

						//0% Effective
			if (col.gameObject.GetComponentInParent<TutShowPatient> ().Efficiency == 0) {
				if (this.GetComponent<Ingredient> ().HeartRate == -1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (3, 5));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (11, 14));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= (Random.Range (20, 23));
				}
				if (this.GetComponent<Ingredient> ().HeartRate <= -4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate -= 100;
				}
	
				if (this.GetComponent<Ingredient> ().HeartRate == 0) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += 0;
				}
	
				if (this.GetComponent<Ingredient> ().HeartRate == 1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (3, 5));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (11, 14));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += (Random.Range (20, 23));
				}
				if (this.GetComponent<Ingredient> ().HeartRate >= 4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().HeartRate += 100;
				}
			}
		

												//SKIN COLOUR
				//100% Effective
			if (col.gameObject.GetComponentInParent<TutShowPatient> ().Efficiency == 2) {
				if (this.GetComponent<Ingredient> ().SkinTone == -1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 5;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 10;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 15;
				}
				if (this.GetComponent<Ingredient> ().SkinTone <= -4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 120;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 0) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 0;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 5;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 10;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 15;
				}
				if (this.GetComponent<Ingredient> ().SkinTone >= 4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 120;
				}
			}
			//50% Effective
			if (col.gameObject.GetComponentInParent<TutShowPatient> ().Efficiency == 1) {
				if (this.GetComponent<Ingredient> ().SkinTone == -1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 3;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 7;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 12;
				}
				if (this.GetComponent<Ingredient> ().SkinTone <= -4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 120;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 0) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 0;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 3;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 7;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 12;
				}
				if (this.GetComponent<Ingredient> ().SkinTone >= 4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 120;
				}
			}
			//0% Effective
			if (col.gameObject.GetComponentInParent<TutShowPatient> ().Efficiency == 0) {
				if (this.GetComponent<Ingredient> ().SkinTone == -1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 1;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 5;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 10;
				}
				if (this.GetComponent<Ingredient> ().SkinTone <= -4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour -= 120;
				}
					
				if (this.GetComponent<Ingredient> ().SkinTone == 0) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 0;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 1) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 1;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 2) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 5;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 3) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 10;
				}
				if (this.GetComponent<Ingredient> ().SkinTone >= 4) {
					col.gameObject.GetComponentInParent<TutShowPatient> ().SkinColour += 120;
				}
			}
		}
	}
}
