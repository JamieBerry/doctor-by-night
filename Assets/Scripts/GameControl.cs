﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {

	public GameObject PatientObject;
	public GameObject PatientSpawn;

	public int PatientKilled;
	public int PatientSaved;

	public Text PatientSavedText;

	public Image Skull1, Skull2;

	public AudioSource SavedSound;
	public AudioSource DeathSound;

	void Start () {
		PatientSaved = 0;
		PatientKilled = 0;
		RespawnPatient ();
		Skull1.enabled = false;
		Skull2.enabled = false;	

		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty != 2) {
			SpawnExtraIngridients ();
		}
	}
	
	void Update () {
		PatientSavedText.text = "Patients Saved: " + PatientSaved.ToString();
		if (PatientKilled == 3) {
			SceneManager.LoadScene (2);
		}

		if (PatientKilled == 1) {
			Skull1.enabled = true;
		}

		if (PatientKilled == 2){
			Skull2.enabled = true;
		}

		GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().SavedPeople = PatientSaved;
		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty == 3) {
			if (PatientSaved >= 10) {
				GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().HardUnlocked = true;
			}
		}
	}

	public void RespawnPatient(){
		Instantiate (PatientObject, PatientSpawn.transform.position, PatientSpawn.transform.rotation);
	}

	public void SpawnExtraIngridients(){
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnLiquid (1);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnLiquid (2);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (8);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (9);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (10);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (11);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (12);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (13);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (14);
		GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (15);
	}

	public void PlaySavedSound(){
		SavedSound.PlayOneShot (SavedSound.clip);
	}

	public void PlayDeathSound(){
		DeathSound.PlayOneShot (DeathSound.clip);
	}

}
