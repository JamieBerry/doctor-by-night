﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breaking : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Floor") {
			Destroy (gameObject);
		}
	}
}
