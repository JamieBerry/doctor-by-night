﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mixture : MonoBehaviour {

//	public Text HR;
//	public Text ST;
//	public Text Temp;

	void Start(){
//		HR = GameObject.Find ("HeartRateText").GetComponent<Text>();
//		ST = GameObject.Find ("SkinToneText").GetComponent<Text> ();
//		Temp = GameObject.Find ("TemperatureText").GetComponent<Text>();
	}

	void Update(){

		if (this.GetComponent<Ingredient> ().Temperature == 0) {
//			Temp.text = "Temperature: Cold";
//			Temp.enabled = true;
		}
		if (this.GetComponent<Ingredient> ().Temperature == 1) {
//			Temp.text = "Temperature: Warm";
//			Temp.enabled = true;
		}
		if (this.GetComponent<Ingredient> ().Temperature == 2) {
//			Temp.text = "Temperature: Hot";
//			Temp.enabled = true;
		}

		if (this.GetComponent<Ingredient> ().HeartRate == 1) {
//			HR.text = "HeartRate: +";
		}
		if (this.GetComponent<Ingredient> ().HeartRate == 2) {
//			HR.text = "HeartRate: + +";
		}
		if (this.GetComponent<Ingredient> ().HeartRate == 3) {
//			HR.text = "HeartRate: + + +";
		}
		if (this.GetComponent<Ingredient> ().HeartRate >= 4) {
//			HR.text = "HeartRate: + + + +";
		}
		if (this.GetComponent<Ingredient> ().HeartRate == 0) {
//			HR.text = "HeartRate: 0";
		}
		if (this.GetComponent<Ingredient> ().HeartRate == -1) {
//			HR.text = "HeartRate: -";
		}
		if (this.GetComponent<Ingredient> ().HeartRate == -2) {
//			HR.text = "HeartRate: - -";
		}
		if (this.GetComponent<Ingredient> ().HeartRate == -3) {
//			HR.text = "HeartRate: - - -";
		}
		if (this.GetComponent<Ingredient> ().HeartRate <= -4) {
//			HR.text = "HeartRate: - - - -";
		}

		if (this.GetComponent<Ingredient> ().SkinTone == 1) {
//			ST.text = "Skin Tone: +";
		}
		if (this.GetComponent<Ingredient> ().SkinTone == 2) {
//			ST.text = "Skin Tone: + +";
		}
		if (this.GetComponent<Ingredient> ().SkinTone == 3) {
//			ST.text = "Skin Tone: + + +";
		}
		if (this.GetComponent<Ingredient> ().SkinTone >= 4) {
//			ST.text = "Skin Tone: + + + +";
		}
		if (this.GetComponent<Ingredient> ().SkinTone == 0) {
//			ST.text = "Skin Tone: 0";
		}
		if (this.GetComponent<Ingredient> ().SkinTone == -1) {
//			ST.text = "Skin Tone: -";
		}
		if (this.GetComponent<Ingredient> ().SkinTone == -2) {
//			ST.text = "Skin Tone: - -";
		}
		if (this.GetComponent<Ingredient> ().SkinTone == -3) {
//			ST.text = "Skin Tone: - - -";
		}
		if (this.GetComponent<Ingredient> ().SkinTone <= -4) {
//			ST.text = "Skin Tone: - - - -";
		}
	}

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Dead") {

			Destroy (gameObject);

//			HR.text = "";
//			ST.text = "";
//			Temp.text = "";


			if (this.GetComponent<Ingredient> ().Temperature == 0) {
				col.gameObject.GetComponentInParent<ShowPatient> ().Temperature -= 2;
			}
			if (this.GetComponent<Ingredient> ().Temperature == 1) {
				col.gameObject.GetComponentInParent<ShowPatient> ().Temperature -= 0;
			}

			if (this.GetComponent<Ingredient> ().Temperature == 2) {
				col.gameObject.GetComponentInParent<ShowPatient> ().Temperature += 2;
			}
																//HEART RATE
					//100% Effective
			if (col.gameObject.GetComponentInParent<ShowPatient> ().Efficiency == 2) {
				if (this.GetComponent<Ingredient> ().HeartRate == -1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (7, 10));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (17, 20));

				}
				if (this.GetComponent<Ingredient> ().HeartRate == -3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (27, 30));

				}
				if (this.GetComponent<Ingredient> ().HeartRate <= -4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= 100;
				}

				if (this.GetComponent<Ingredient> ().HeartRate == 0) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += 0;
				}

				if (this.GetComponent<Ingredient> ().HeartRate == 1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (7, 10));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (17, 20));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (27, 30));
				}
				if (this.GetComponent<Ingredient> ().HeartRate >= 4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += 100;
				}
			}
					//50% Effective
			if (col.gameObject.GetComponentInParent<ShowPatient> ().Efficiency == 1) {
				if (this.GetComponent<Ingredient> ().HeartRate == -1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (5, 7));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (14, 17));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (23, 27));
				}
				if (this.GetComponent<Ingredient> ().HeartRate <= -4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= 100;
				}

				if (this.GetComponent<Ingredient> ().HeartRate == 0) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += 0;
				}
	
				if (this.GetComponent<Ingredient> ().HeartRate == 1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (5, 7));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (14, 17));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (23, 27));
				}
				if (this.GetComponent<Ingredient> ().HeartRate >= 4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += 100;
				}
			}

						//0% Effective
			if (col.gameObject.GetComponentInParent<ShowPatient> ().Efficiency == 0) {
				if (this.GetComponent<Ingredient> ().HeartRate == -1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (3, 5));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (11, 14));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == -3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= (Random.Range (20, 23));
				}
				if (this.GetComponent<Ingredient> ().HeartRate <= -4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate -= 100;
				}
	
				if (this.GetComponent<Ingredient> ().HeartRate == 0) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += 0;
				}
	
				if (this.GetComponent<Ingredient> ().HeartRate == 1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (3, 5));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (11, 14));
				}
				if (this.GetComponent<Ingredient> ().HeartRate == 3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += (Random.Range (20, 23));
				}
				if (this.GetComponent<Ingredient> ().HeartRate >= 4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().HeartRate += 100;
				}
			}
		

												//SKIN COLOUR
				//100% Effective
			if (col.gameObject.GetComponentInParent<ShowPatient> ().Efficiency == 2) {
				if (this.GetComponent<Ingredient> ().SkinTone == -1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 5;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 10;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 15;
				}
				if (this.GetComponent<Ingredient> ().SkinTone <= -4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 120;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 0) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 0;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 5;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 10;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 15;
				}
				if (this.GetComponent<Ingredient> ().SkinTone >= 4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 120;
				}
			}
			//50% Effective
			if (col.gameObject.GetComponentInParent<ShowPatient> ().Efficiency == 1) {
				if (this.GetComponent<Ingredient> ().SkinTone == -1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 3;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 7;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 12;
				}
				if (this.GetComponent<Ingredient> ().SkinTone <= -4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 120;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 0) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 0;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 3;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 7;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 12;
				}
				if (this.GetComponent<Ingredient> ().SkinTone >= 4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 120;
				}
			}
			//0% Effective
			if (col.gameObject.GetComponentInParent<ShowPatient> ().Efficiency == 0) {
				if (this.GetComponent<Ingredient> ().SkinTone == -1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 1;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 5;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == -3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 10;
				}
				if (this.GetComponent<Ingredient> ().SkinTone <= -4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour -= 120;
				}
					
				if (this.GetComponent<Ingredient> ().SkinTone == 0) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 0;
				}

				if (this.GetComponent<Ingredient> ().SkinTone == 1) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 1;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 2) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 5;
				}
				if (this.GetComponent<Ingredient> ().SkinTone == 3) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 10;
				}
				if (this.GetComponent<Ingredient> ().SkinTone >= 4) {
					col.gameObject.GetComponentInParent<ShowPatient> ().SkinColour += 120;
				}
			}
		}
	}
}
