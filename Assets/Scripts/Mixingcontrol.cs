﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mixingcontrol : MonoBehaviour {

	public GameObject Mixture;

	public bool InArea;
	public bool StartMix;
	public bool MixDone;

	public int HeartRateTotal;
	public int SkinToneTotal;
	public int TempTotal;
	public int IngredientTotal;

	public float MixingCountDown = 5;

	public Slider CookingSlider;

	public Text ProgressText;
	public Text DoneText;

	public bool CountingDown;
	public bool CanBeMixed;
	public bool MixingNow;
	public bool MadeMix;

	public float ColBaseR,ColBaseG,ColBaseB;
	public float ColR, ColG, ColB;

	void OnTriggerEnter(Collider col){
		if ((col.gameObject.tag == "Player")) {
			InArea = true;
			DoneText.enabled = true;
			ProgressText.enabled = true;

		}
	}

	void OnTriggerExit(Collider col){
		if ((col.gameObject.tag == "Player")) {
			InArea = false;
			DoneText.enabled = false;
			ProgressText.enabled = false;
		}
	}

	void Update(){
		if (IngredientTotal == 1) {
			ProgressText.text = "Press E to start mixing the ingredients";
			CanBeMixed = true;
		}

		if (IngredientTotal == 2) {
			GameObject.Find ("MixLiquid").GetComponent<MeshCollider> ().enabled = false;
			ProgressText.text = "Press E to start mixing the ingredients";

			CanBeMixed = true;
		}

		if (CanBeMixed == true) {
			if ((Input.GetKeyDown (KeyCode.E)) && MixingNow == false && MixDone == false) {
				MixingNow = true;
				GameObject.Find ("MixLiquid").GetComponent<MeshCollider> ().enabled = false;

			}
		}

		if (MixingNow == true && MixDone == false) {
			Invoke ("FinishedMix", 5);
			ProgressText.text = "";
			CountingDown = true;
			if (CountingDown == true) {
				CookingSlider.value += 0.2f * Time.deltaTime;
			}
		}

		if (MixingCountDown <= 0) {
			ChooseColour ();
			MixingCountDown = 0;
			CountingDown = false;
			ProgressText.text = "";

		}


		if (MixDone == true && MixingNow == false) {
			ChooseColour ();
			GameObject.Find ("MixLiquid").GetComponentInChildren<MeshRenderer> ().material.color = new Color ((ColBaseR + ColR)/255, (ColBaseG + ColG)/255, (ColBaseB + ColB)/255, 1);
			ProgressText.text = "";
			if (InArea == true) {
				DoneText.text = "Press E to bottle the mixture";
				DoneText.enabled = true;

				if (Input.GetKeyDown (KeyCode.E)) {
					ProduceMix ();
					InitaliseMixture ();
					DoneText.text = "";
				}
			}
		}

	}

	void Start(){
		InitaliseMixture ();
	}

	public void FinishedMix(){
		MixingNow = false;
		MixDone = true;
		CancelInvoke ();
	}

	public void ProduceMix(){
		GameObject Temp = Instantiate (Mixture, new Vector3 (-24.5f, 1.4f, 2.16f), Quaternion.Euler(-90,0,0)) as GameObject;

		Temp.GetComponent<Ingredient> ().HeartRate = HeartRateTotal;
		Temp.GetComponent<Ingredient> ().SkinTone = SkinToneTotal;

		if (GameObject.Find ("MixLiquid").GetComponent<IngridientMixing> ().LowTemp == true){
			Temp.GetComponent<Ingredient> ().Temperature = 0;
		}
		if (GameObject.Find ("MixLiquid").GetComponent<IngridientMixing> ().MediumTemp == true) {
			Temp.GetComponent<Ingredient> ().Temperature = 1;
		}
		if (GameObject.Find ("MixLiquid").GetComponent<IngridientMixing> ().HighTemp == true) {
			Temp.GetComponent<Ingredient> ().Temperature = 2;
		}
	}

	public void InitaliseMixture(){
		IngredientTotal = 0;
		HeartRateTotal = 0;
		SkinToneTotal = 0;
		TempTotal = 0;
		MixDone = false;
		CanBeMixed = false;
		MixingNow = false;
		GameObject.Find ("MixLiquid").GetComponent<MeshCollider> ().enabled = true;
		GameObject.Find ("MixLiquid").GetComponent<MeshRenderer> ().enabled = false;
		MixingCountDown = 5;
		GameObject.Find ("MixLiquid").GetComponent<IngridientMixing> ().InitaliseTemp();
		GameObject.Find ("MixLiquid").GetComponent<IngridientMixing> ().NoLiquid = true;
		GameObject.Find ("MixLiquid").GetComponent<IngridientMixing> ().WaterLiquid = false;
		GameObject.Find ("MixLiquid").GetComponent<IngridientMixing> ().OilLiquid = false;
		GameObject.Find ("MixLiquid").GetComponent<IngridientMixing> ().AlcoholLiquid = false;
		ProgressText.text = "";
		ProgressText.enabled = true;
		CookingSlider.value = 0;
	}


	public void ChooseColour(){
		if (SkinToneTotal == -4) {
			ColBaseR = 162;
			ColBaseG = 24;
			ColBaseB = 160;
		}
		if (SkinToneTotal == -3) {
			ColBaseR = 170;
			ColBaseG = 34;
			ColBaseB = 76;
		}
		if (SkinToneTotal == -2) {
			ColBaseR = 174;
			ColBaseG = 40;
			ColBaseB = 36;
		}
		if (SkinToneTotal == -1) {
			ColBaseR = 180;
			ColBaseG = 56;
			ColBaseB = 54;
		}
		if (SkinToneTotal == 0) {
			ColBaseR = 176;
			ColBaseG = 132;
			ColBaseB = 26;
		}
		if (SkinToneTotal == 1) {
			ColBaseR = 172;
			ColBaseG = 184;
			ColBaseB = 44;
		}
		if (SkinToneTotal == 2) {
			ColBaseR = 180;
			ColBaseG = 200;
			ColBaseB = 16;
		}
		if (SkinToneTotal == 3) {
			ColBaseR = 94;
			ColBaseG = 170;
			ColBaseB = 14;
		}
		if (SkinToneTotal == 4) {
			ColBaseR = 10;
			ColBaseG = 130;
			ColBaseB = 10;
		}


		if (HeartRateTotal == -4) {
			ColR = -8;
			ColG = -80;
			ColB = -8;
		}
		if (HeartRateTotal == -3) {
			ColR = -6;
			ColG = -60;
			ColB = -6;
		}
		if (HeartRateTotal == -2) {
			ColR = -4;
			ColG = -40;
			ColB = -4;
		}
		if (HeartRateTotal == -1) {
			ColR = -2;
			ColG = -20;
			ColB = -2;
		}
		if (HeartRateTotal == 0) {
			ColR = 0;
			ColG = 0;
			ColB = 0;
		}
		if (HeartRateTotal == 1) {
			ColR = 2;
			ColG = 20;
			ColB = 2;
		}
		if (HeartRateTotal == 2) {
			ColR = 4;
			ColG = 40;
			ColB = 4;
		}
		if (HeartRateTotal == 3) {
			ColR = 6;
			ColG = 60;
			ColB = 6;
		}
		if (HeartRateTotal == 4) {
			ColR = 8;
			ColG = 80;
			ColB = 8;
		}


		//PATTERN IS +/-2,+/-20,+/-2
		//AT HR = 0
		//SK4 = 10,130,10
		//SK3 = 94,170,14
		//SK2 = 180,200,16
		//SK1 = 172,184,44
		//SK0 = 176,132,26
		//SK-1 = 180,56,54
		//SK-2 = 174,40,36
		//SK-3 = 170,34,76
		//SK-4 = 162,24,160
	}

}