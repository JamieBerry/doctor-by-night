﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour {

	public Button EasyBut, MediumBut, HardBut;
	public Image DescBackground;
	public Text EasyDesc, MediumDesc, HardDesc;
	public AudioSource Ding;

	public GameObject HardUnlockText;

	public GameObject Credits;
	public GameObject Exitbut;
	public GameObject BackBut;
	public GameObject HardDescription;

	public Text TutDesc;

	void Start(){
		DescBackground.enabled = false;
		EasyDesc.enabled = false;
		MediumDesc.enabled = false;
		HardDesc.enabled = false;
		TutDesc.enabled = false;

		GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().SavedPeople = 0;

		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().HardUnlocked == true) {
			HardUnlockText.SetActive (false);
			HardBut.interactable = true;
			HardDescription.SetActive (true);
		}
	}

	public void ShowTutRollover(){
		DescBackground.enabled = true;
		TutDesc.enabled = true;
	}

	public void HideTutRollover(){
		DescBackground.enabled = false;
		TutDesc.enabled = false;
	}

	public void ShowEasyRollover(){
		DescBackground.enabled = true;
		EasyDesc.enabled = true;
	}
	public void HideEasyRollover(){
		DescBackground.enabled = false;
		EasyDesc.enabled = false;
	}


	public void ShowMediumRollover(){
		DescBackground.enabled = true;
		MediumDesc.enabled = true;
	}
	public void HideMediumRollover(){
		DescBackground.enabled = false;
		MediumDesc.enabled = false;
	}


	public void ShowHardRollover(){
		DescBackground.enabled = true;
		HardDesc.enabled = true;
	}
	public void HideHardRollover(){
		DescBackground.enabled = false;
		HardDesc.enabled = false;
	}

	public void StartEasy(){
		Ding.PlayOneShot (Ding.clip);
		GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty = 2;
		SceneManager.LoadScene (1);
	}

	public void StartMedium(){
		Ding.PlayOneShot (Ding.clip);
		GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty = 3;
		SceneManager.LoadScene (1);

	}

	public void StartHard(){
		Ding.PlayOneShot (Ding.clip);
		GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty = 4;
		SceneManager.LoadScene (1);
	}

	public void StartTutorial(){
		Ding.PlayOneShot (Ding.clip);
		GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty = 1;
		SceneManager.LoadScene (3);
	}
	public void ShowCredits(){
		Ding.PlayOneShot (Ding.clip);
		Credits.SetActive (true);
		BackBut.SetActive (true);
		Exitbut.SetActive (false);
	}

	public void HideCredits(){
		Ding.PlayOneShot (Ding.clip);
		Credits.SetActive (false);
		BackBut.SetActive (false);
		Exitbut.SetActive (true);
	}

	public void ExitGame(){
		Application.Quit ();
	}

}
