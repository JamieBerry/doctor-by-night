﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawns : MonoBehaviour {

	public GameObject[] RespawnLiquidObject;		//Water = -21.18,1.3,3.08		Oil = -21.2,1.3,2.88		Alcohol = -21.19,1.3,2.66
	public GameObject[] RespawnIngridientsObjects;

	public Transform[] RespawnLiquidPosition;
	public Transform[] RespawnIngridientsPositions;

	public int IngredientNo;
		
	public void RespawnIngridient (int ingridientNo){
		Instantiate (RespawnIngridientsObjects [ingridientNo], RespawnIngridientsPositions [ingridientNo].transform.position, RespawnIngridientsPositions [ingridientNo].transform.rotation);
	}

	public void RespawnLiquid(int liquid){
		Instantiate (RespawnLiquidObject [liquid], RespawnLiquidPosition [liquid].transform.position, RespawnLiquidPosition [liquid].transform.rotation);
	}

	public void RespawnIngredientWITHDELAY(){
		StartCoroutine ("WaitSpawn");
		}

	public IEnumerator WaitSpawn(){
		yield return(new WaitForSeconds (20));
		Instantiate (RespawnIngridientsObjects [IngredientNo], RespawnIngridientsPositions [IngredientNo].transform.position, RespawnIngridientsPositions [IngredientNo].transform.rotation);
	}


}
