﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyDropped : MonoBehaviour {

	public GameObject glassShatter;

	/*void Start () {

		glassShatter = GetComponent<AudioSource> ();

	}*/

	void OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "Floor") {

			glassShatter.SetActive (true);

			Destroy (gameObject, 2.0f);
		}
	}
}
