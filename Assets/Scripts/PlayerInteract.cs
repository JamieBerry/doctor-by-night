﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerInteract : MonoBehaviour {
	
	public GameObject Notebook;
	public GameObject Easybook;

	public int CurrentPage;				//0 = plants, 1 = minerals, 2 = insects, 3 = animals

	public GameObject PlantPage;
	public GameObject MineralPage;
	public GameObject InsectPage;
	public GameObject AnimalPage;

	public GameObject EASYPlantPage;
	public GameObject EASYMineralPage;

	public Transform HoldingAreaTransform;
	public Vector3 HoldingArea;

	public Transform Paren;

	public LayerMask Pickups;
	private GameObject[] PickupObject;

	public bool Pickupable;

	public Text ItemText;

	public bool NotebookOpen;


	void Start () {
		Pickupable = false;
		HoldingAreaTransform = GameObject.Find ("HoldingHand").transform;										//Sets holding area transform
		HoldingArea = GameObject.Find ("HoldingHand").transform.position;										//SetsHolding area vector3
		PickupObject = GameObject.FindGameObjectsWithTag ("Pickup");											//Sets all the pickups in an array
		Notebook.SetActive (false);
		Easybook.SetActive (false);
		Cursor.lockState = CursorLockMode.Locked;
		CurrentPage = 0;
		NotebookOpen = false;

	}




	void Update () {

		if (NotebookOpen == false) {
			Notebook.SetActive (false);
			Easybook.SetActive (false);
			ItemText.enabled = true;
		}

		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty == 2) {
			if (Input.GetKey (KeyCode.Tab)) {
				NotebookOpen = true;
				if (NotebookOpen == true) {
					
				
					Easybook.SetActive (true);
					ItemText.enabled = false;

					if (Input.GetButtonDown ("Fire2")) {
						CurrentPage += 1;
					}
					if (Input.GetButtonDown ("Fire1")) {
						CurrentPage -= 1;
					}

					if (CurrentPage < 0) {
						CurrentPage = 0;
					}
					if (CurrentPage > 1) {
						CurrentPage = 1;
					}

					if (CurrentPage == 0) {
						EASYPlantPage.SetActive (true);
						EASYMineralPage.SetActive (false);
					}

					if (CurrentPage == 1) {
						EASYPlantPage.SetActive (false);
						EASYMineralPage.SetActive (true);
					}

				} 
			} else {
					NotebookOpen = false;
//					Easybook.SetActive (false);
//					ItemText.enabled = true;
				}
			}
//		}


		//NORMAL NOTEBOOK WITH HR AND ST IF NOT ON EASY
		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty != 2) {
			if (Input.GetKey (KeyCode.Tab)) {
				
				NotebookOpen = true;
				if (NotebookOpen == true) {
				
					Notebook.SetActive (true);
					ItemText.enabled = false;

					if (Input.GetButtonDown ("Fire2")) {
						CurrentPage += 1;
					}
					if (Input.GetButtonDown ("Fire1")) {
						CurrentPage -= 1;
					}

					if (CurrentPage < 0) {
						CurrentPage = 0;
					}
					if (CurrentPage > 3) {
						CurrentPage = 3;
					}

					if (CurrentPage == 0) {
						PlantPage.SetActive (true);
						MineralPage.SetActive (false);
						InsectPage.SetActive (false);
						AnimalPage.SetActive (false);
					}

					if (CurrentPage == 1) {
						PlantPage.SetActive (false);
						MineralPage.SetActive (true);
						InsectPage.SetActive (false);
						AnimalPage.SetActive (false);
					}

					if (CurrentPage == 2) {
						PlantPage.SetActive (false);
						MineralPage.SetActive (false);
						InsectPage.SetActive (true);
						AnimalPage.SetActive (false);
					}

					if (CurrentPage == 3) {
						PlantPage.SetActive (false);
						MineralPage.SetActive (false);
						InsectPage.SetActive (false);
						AnimalPage.SetActive (true);
					}
				}
			}else {
//					Notebook.SetActive (false);
//					ItemText.enabled = true;
					NotebookOpen = false;

				}
			}
//		}

		HoldingAreaTransform = GameObject.Find ("HoldingHand").transform;										//Updates holding area tranform
		HoldingArea = GameObject.Find ("HoldingHand").transform.position;										//Updates holding area vector3

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);											//Creates ray from mouse
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, 2, Pickups)) {														//Projects ray 2 distance and looks for pickup layer only
			Debug.DrawLine (ray.origin, hit.point);																//Draws line to help me know what is hit and when
			GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().color = Color.blue;					//Turns crosshair blue when over a pickupable item
			Pickupable = true;
			ItemText.text = hit.collider.GetComponentInChildren<NameScript> ().IngridientName;
		} else {
			Pickupable = false;
			GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().color = Color.green;					//Turns crosshair green when not over a pickupable item
			ItemText.text = "";

			foreach (GameObject obj in PickupObject) {
				if (obj != null) {
					obj.GetComponent<Gravity> ().GravityON ();
				}
			}
		}

		if(Pickupable == true && Input.GetButtonDown("Fire1")){
			hit.transform.position = HoldingArea;
			hit.rigidbody.useGravity = false;
			hit.rigidbody.isKinematic = true;
			hit.transform.SetParent (HoldingAreaTransform);
			GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().enabled = false;
		}
		if(Pickupable == true && Input.GetButtonDown("Fire2")){
			hit.rigidbody.useGravity = true;
			hit.rigidbody.isKinematic = false;
			GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().enabled = true;
		}

		if(Pickupable == false){
			HoldingAreaTransform.DetachChildren ();
			GameObject.Find ("Crosshair").GetComponentInChildren<Image> ().enabled = true;
		}



	}
}
