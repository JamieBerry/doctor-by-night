﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngridientMixing : MonoBehaviour {

	public bool NoLiquid;

	public bool WaterLiquid;
	public bool OilLiquid;
	public bool AlcoholLiquid;

	public bool HighTemp;
	public bool MediumTemp;
	public bool LowTemp;

	public bool RaiseTemp;
	public bool LowerTemp;

	public float TemperatureMeter;

	public Image Low1, Low2, Low3, Low4;
	public Image Medium1, Medium2, Medium3, Medium4;
	public Image High1, High2, High3, High4;

	public ParticleSystem Smoke;
	private ParticleSystem Fire;

	public AudioSource BubblingSounds;
	public AudioSource IngridientAdded;

	void Start () {
		TemperatureMeter = 0;
		InitaliseTemp ();
		NoLiquid = true;
		this.GetComponent<MeshRenderer> ().enabled = false;

		Smoke = GetComponentInChildren<ParticleSystem> ();
		var SLOT = Smoke.colorOverLifetime;
		var SE = Smoke.emission;
		SE.enabled = false;

		Fire = GameObject.Find ("Fires").GetComponentInChildren<ParticleSystem> ();

	}

	void OnTriggerEnter(Collider col){
		if (NoLiquid == true) {
			BubblingSounds.Stop ();
			if (col.gameObject.GetComponentInChildren<Liquid> ().Water == true) {
				Destroy (col.gameObject);
				this.GetComponent<MeshRenderer> ().enabled = true;
				this.GetComponent<MeshRenderer> ().material.color = Color.blue;

				NoLiquid = false;
				WaterLiquid = true;

				GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnLiquid (0);
			}

			if (col.gameObject.GetComponentInChildren<Liquid> ().Oil == true) {
				Destroy (col.gameObject);
				this.GetComponent<MeshRenderer> ().enabled = true;
				this.GetComponent<MeshRenderer> ().material.color = Color.black;

				NoLiquid = false;
				OilLiquid = true;

				GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnLiquid (1);

			} 

			if (col.gameObject.GetComponentInChildren<Liquid> ().Alcohol == true) {
				Destroy (col.gameObject);
				this.GetComponent<MeshRenderer> ().enabled = true;
				this.GetComponent<MeshRenderer> ().material.color = new Color(0.59f,0.34f,0.06f);

				NoLiquid = false;
				AlcoholLiquid = true;

				GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnLiquid (2);

			}
		}

		if (NoLiquid == false) {
			BubblingSounds.PlayOneShot (BubblingSounds.clip);
			BubblingSounds.loop = true;
			var SE = Smoke.emission;
			SE.enabled = true;

			if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty != 4) {
				//WATER
				if (col.gameObject.GetComponentInChildren<Ingredient> () && WaterLiquid == true) {
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().HeartRateTotal += col.gameObject.GetComponentInChildren<Ingredient> ().HeartRate;
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().IngredientTotal += 1;
					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (col.gameObject.GetComponentInChildren<Ingredient> ().RespawnID);
					Destroy (col.gameObject);
					IngridientAdded.PlayOneShot (IngridientAdded.clip);
				}
				//OIL
				if (col.gameObject.GetComponentInChildren<Ingredient> () && OilLiquid == true) {
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().HeartRateTotal += col.gameObject.GetComponentInChildren<Ingredient> ().HeartRate;
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().SkinToneTotal += col.gameObject.GetComponentInChildren<Ingredient> ().SkinTone;
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().IngredientTotal += 1;
					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (col.gameObject.GetComponentInChildren<Ingredient> ().RespawnID);
					Destroy (col.gameObject);
					IngridientAdded.PlayOneShot (IngridientAdded.clip);
				}
				//ALCOHOL
				if (col.gameObject.GetComponentInChildren<Ingredient> () && AlcoholLiquid == true) {
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().SkinToneTotal += col.gameObject.GetComponentInChildren<Ingredient> ().SkinTone;
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().IngredientTotal += 1;
					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngridient (col.gameObject.GetComponentInChildren<Ingredient> ().RespawnID);
					Destroy (col.gameObject);
					IngridientAdded.PlayOneShot (IngridientAdded.clip);
				}
			}

			if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty == 4) {
				//WATER
				if (col.gameObject.GetComponentInChildren<Ingredient> () && WaterLiquid == true) {
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().HeartRateTotal += col.gameObject.GetComponentInChildren<Ingredient> ().HeartRate;
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().IngredientTotal += 1;

					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().IngredientNo = (col.gameObject.GetComponentInChildren<Ingredient> ().RespawnID);
					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngredientWITHDELAY ();

//					if (GameObject.Find ("GameController").GetComponentInChildren<HardController> ().Ingredient1 >= 16) {
//						GameObject.Find ("GameController").GetComponentInChildren<HardController> ().Ingredient1 = (col.gameObject.GetComponentInChildren<Ingredient> ().RespawnID);
//					} else {
//						GameObject.Find ("GameController").GetComponentInChildren<HardController> ().Ingredient2 = (col.gameObject.GetComponentInChildren<Ingredient> ().RespawnID);
//					}

					Destroy (col.gameObject);
					IngridientAdded.PlayOneShot (IngridientAdded.clip);
				}
				//OIL
				if (col.gameObject.GetComponentInChildren<Ingredient> () && OilLiquid == true) {
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().HeartRateTotal += col.gameObject.GetComponentInChildren<Ingredient> ().HeartRate;
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().SkinToneTotal += col.gameObject.GetComponentInChildren<Ingredient> ().SkinTone;
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().IngredientTotal += 1;

					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().IngredientNo = (col.gameObject.GetComponentInChildren<Ingredient> ().RespawnID);
					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngredientWITHDELAY ();

					Destroy (col.gameObject);
					IngridientAdded.PlayOneShot (IngridientAdded.clip);
				}
				//ALCOHOL
				if (col.gameObject.GetComponentInChildren<Ingredient> () && AlcoholLiquid == true) {
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().SkinToneTotal += col.gameObject.GetComponentInChildren<Ingredient> ().SkinTone;
					GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().IngredientTotal += 1;

					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().IngredientNo = (col.gameObject.GetComponentInChildren<Ingredient> ().RespawnID);
					GameObject.Find ("RespawnManager").GetComponentInChildren<Respawns> ().RespawnIngredientWITHDELAY ();

					Destroy (col.gameObject);
					IngridientAdded.PlayOneShot (IngridientAdded.clip);
				}
			}
		}
	}
		
	void Update () {
		if (NoLiquid == true) {
		}



		if (NoLiquid == false) {
			TemperatureInterface ();

			if (GameObject.Find ("MixingBowl").GetComponent<Mixingcontrol> ().InArea == true) {

				if (Input.GetKeyDown (KeyCode.Q)) {
				TemperatureMeter += 4;
				LowerTemp = true;
			}
		}

			if (LowerTemp == true) {
				TemperatureMeter -= 5 * Time.deltaTime;
			}
			if (TemperatureMeter < 0) {
				TemperatureMeter = 0;
			}
			if (TemperatureMeter > 110) {
				TemperatureMeter = 110;
			}
		}
	}



		public void TemperatureInterface(){

		var FireEmission = Fire.emission;

		var SmokeColor = Smoke.colorOverLifetime;
		SmokeColor.enabled = true;
		Gradient grad = new Gradient ();
		//1/255 then * by temp
		//ADD 17.5 to each temperature

		if (TemperatureMeter <= 0) {
			LowTemp = true;
			Low1.enabled = true;
			Low2.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.18f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 45 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.40f;

			FireEmission.rateOverTime = 100.0f;
		}
		if (TemperatureMeter > 0 && TemperatureMeter <= 10) {
			LowTemp = true;
			Low2.enabled = true;
			Low3.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.25f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 62.5 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.40f;

			FireEmission.rateOverTime = 100.0f;

		}
		if (TemperatureMeter > 10 && TemperatureMeter <= 20) {
			LowTemp = true;
			Low3.enabled = true;
			Low4.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.31f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 80 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.50f;

			FireEmission.rateOverTime = 100.0f;

		}
		if (TemperatureMeter > 20 && TemperatureMeter <= 30) {
			LowTemp = true;
			MediumTemp = false;
			Low4.enabled = true;
			Medium1.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.38f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 97.5 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.50f;

			FireEmission.rateOverTime = 100.0f;

		}

		if (TemperatureMeter > 30 && TemperatureMeter <= 40) {
			MediumTemp = true;
			LowTemp = false;
			Medium1.enabled = true;
			Medium2.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.45f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 115 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.60f;

			FireEmission.rateOverTime = 200.0f;

		}
		if (TemperatureMeter > 40 && TemperatureMeter <= 50) {
			MediumTemp = true;
			Medium2.enabled = true;
			Medium3.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.52f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 132.5 alpha
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.60f;

			FireEmission.rateOverTime = 200.0f;

		}
		if (TemperatureMeter > 50 && TemperatureMeter <= 60) {
			MediumTemp = true;
			Medium3.enabled = true;
			Medium4.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.59f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 150 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.70f;

			FireEmission.rateOverTime = 200.0f;

		}
		if (TemperatureMeter > 60 && TemperatureMeter <= 70) {
			MediumTemp = true;
			HighTemp = false;
			Medium4.enabled = true;
			High1.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.66f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 167.5 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.70f;

			FireEmission.rateOverTime = 200.0f;

		}

		if (TemperatureMeter > 70 && TemperatureMeter <= 80) {
			HighTemp = true;
			MediumTemp = false;
			High1.enabled = true;
			High2.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.73f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 185 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.80f;

			FireEmission.rateOverTime = 500.0f;

		}
		if (TemperatureMeter > 80 && TemperatureMeter <= 90) {
			HighTemp = true;
			High2.enabled = true;
			High3.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.80f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 202.5 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.80f;

			FireEmission.rateOverTime = 500.0f;

		}
		if (TemperatureMeter > 90 && TemperatureMeter <= 100) {
			HighTemp = true;
			High3.enabled = true;
			High4.enabled = false;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.86f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 220 alpha
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.90f;

			FireEmission.rateOverTime = 500.0f;

		}
		if (TemperatureMeter > 100 && TemperatureMeter <= 110) {
			HighTemp = true;
			High4.enabled = true;

			grad.SetKeys(new GradientColorKey[] {new GradientColorKey(Color.black,0.0f), new GradientColorKey(Color.clear,1.0f)}, new GradientAlphaKey[] {new GradientAlphaKey(0.93f,0.0f), new GradientAlphaKey(0.0f,1.0f) } );		//Sets smoke to 237.5 alpha 
			SmokeColor.color = grad;

			BubblingSounds.pitch = 0.90f;

			FireEmission.rateOverTime = 500.0f;

		}
	}



	public void InitaliseTemp(){
		BubblingSounds.Stop ();

		TemperatureMeter = 0;
		LowTemp = true;
		MediumTemp = false;
		HighTemp = false;
		Low1.enabled = true;
		Low2.enabled = false;
		Low3.enabled = false;
		Low4.enabled = false;
		Medium1.enabled = false;
		Medium2.enabled = false;
		Medium3.enabled = false;
		Medium4.enabled = false;
		High1.enabled = false;
		High2.enabled = false;
		High3.enabled = false;
		High4.enabled = false;

		var SE = Smoke.emission;
		SE.enabled = false;
		var SmokeColor = Smoke.colorOverLifetime;
		SmokeColor.enabled = false;
	}

}
