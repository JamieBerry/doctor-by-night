﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneControl : MonoBehaviour {

	public Button RestartTheGame;
	public Text Bodycount;


	void Start(){
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;

		Bodycount.text = "Patients Saved: " + GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().SavedPeople.ToString();
	}

	public void RedoGame(){
		SceneManager.LoadScene (0);
	}

	void MainMenu(){

	}
}
