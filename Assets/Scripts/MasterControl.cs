﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterControl : MonoBehaviour {

	public int Difficulty;			//0 = Menu	1 = Tutorial	2 = Easy	3 = Medium	4 = Hard

	public int SavedPeople;

	public bool HardUnlocked;

	public static MasterControl instance;

	void Awake(){
		if (instance) {
			DestroyImmediate (gameObject);
		} else {
			DontDestroyOnLoad (gameObject);
			instance = this;
		}
	}
	void Start () {
		AudioListener.pause = false;
		Difficulty = 0;
		HardUnlocked = false;
	}
}
