﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShowPatient : MonoBehaviour {

	public GameObject PatientText;
	public GameObject Patient;

	public Text PatientInfoText;
	public Text PatientLifeProgress;
	public Text PatientTemperature;

	public int PatientNo;

	public float SkinColour;
	public float Temperature;
	public Slider TemperatureSlide;
	public float HeartRate;
	public float LifeProgress;
	public Slider LifeSlide;

	public int Efficiency;						//0 = 0% Efficient	-	1 = 50% Efficient	-	2 = 100% Efficient			//0 = 50% Life Progress Raise	-	1 = 75% Life Progress Raise	-	2 = Regular Life Progress Raise

	public GameObject Arm;

	public bool GoodHeartRate;
	public bool GoodSkin;

	public GameObject HealthyBreathing;
	public GameObject HeartBeat;
	public GameObject Panting;
	public GameObject Coughing;

	public float DifficultyNum;
	public float FasterSpeeds;

	public int HRStart;
	public int STStart;
	public int TEMPStart;

	void Start(){
		PatientText.SetActive (false);
		Patient = GameObject.Find ("Patient1(Clone)");

		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty == 1) {		//TUTORIAL
			HRStart = 80;
			STStart = 50;
		}
		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty == 2) {		//EASY
			Arm.GetComponent<MeshRenderer> ().material.color = Color.white;
		}

			HRStart = Random.Range (0, 2);
			if (HRStart >= 1) {
				HeartRate = Random.Range (75 - DifficultyNum, 90);
			if (HeartRate <= 61) {
				HeartRate = 61;
			}
		}
			if (HRStart < 1) {
				HeartRate = Random.Range (105, 140 + DifficultyNum);
			if (HeartRate >= 158) {
				HeartRate = 158;
			}
		}

		TEMPStart = Random.Range (0, 2);
		if (TEMPStart >= 1) {
			Temperature = Random.Range (28, 33);
		}
		if (TEMPStart < 1) {
			Temperature = Random.Range (39, 42);
		}



		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty != 2) {		//NOT ON EASY THEN ADD SKINTONE RANDOM
			STStart = Random.Range (0, 2);
			if (STStart >= 1) {
				SkinColour = Random.Range (30 - DifficultyNum, 55);

			if (SkinColour <= 8) {
				SkinColour = 8;
			}
		}
			if (STStart < 1) {
				SkinColour = Random.Range (56, 70 + DifficultyNum);
				if (SkinColour >= 98) {
					SkinColour = 98;
				}
			}
		}

		FasterSpeeds = GameObject.Find ("GameController").GetComponentInChildren<GameControl> ().PatientSaved * 0.0005F;
		DifficultyNum = GameObject.Find ("GameController").GetComponentInChildren<GameControl> ().PatientSaved;
	}

	void Update () {
		Patient = GameObject.Find ("Patient1(Clone)");

		TemperatureSlide.value = Temperature;
		LifeSlide.value = LifeProgress;

		PatientInfoText.text = "BPM: " + Mathf.Round (HeartRate);
		PatientLifeProgress.text = Mathf.Round (LifeProgress) + "%";
		PatientTemperature.text = Mathf.Round (Temperature) + "°C";

		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty != 2) {
			if (GoodHeartRate && GoodSkin) {
				{
					HealthyBreathing.SetActive (true);
					Coughing.SetActive (false);
					LifeBar ();
				}
			}
		} else {
			if (GoodHeartRate) {
				{
					HealthyBreathing.SetActive (true);
					Coughing.SetActive (false);
					LifeBar ();
				}
			}
		}
		if (LifeProgress >= 100) {
				LifeProgress = 0;
				Destroy (Patient);
				NewPatient ();
		}

		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty != 2) {
			if (!GoodHeartRate || !GoodSkin) {
				HealthyBreathing.SetActive (false);
			}
		} else {
			if (!GoodHeartRate) {
				HealthyBreathing.SetActive (false);
			}
		}
			





		//HEART RATE
		if (HeartRate <= 60) {
			PatientInfoText.text = "DEAD";
			KillPatient ();
		}

		if (HeartRate >= 60 && HeartRate <= 70) {										// if above 60 but below 70 play heartbeat
			HeartBeat.SetActive (true);
		}																			

		if (HeartRate >= 60 && HeartRate <= 75) {
			HeartBeat.SetActive (false);												// stops heartbeat sound
			Invoke ("LowerHeartRate", 0);
			if (LifeProgress > 0) {
				Invoke ("LowerLifeProgress", 0);
			}
		}
		if (HeartRate >= 75 && HeartRate <= 99.5f) {
			Invoke ("LowerHeartRateFast", 0);
		}
		if (HeartRate >= 95 && HeartRate <= 105) {
			GoodHeartRate = true;
		} else {
			GoodHeartRate = false;
		}
		if (HeartRate >= 99.5f && HeartRate <= 145) {
			Invoke ("RaiseHeartRateFast", 0);
		}
		if (HeartRate >= 145 && HeartRate <= 160) {
			HeartBeat.SetActive (false);												// stops heartbeat sound
			Invoke ("RaiseHeartRate", 0);
			if (LifeProgress > 0) {
				Invoke ("LowerLifeProgress", 0);
			}
		}

		if (HeartRate >= 150 && HeartRate <= 160) {										// if below 160 but above 150 play heartbeat
			HeartBeat.SetActive (true);
		}																			

		if (HeartRate >= 70 && HeartRate <= 150) {
			HeartBeat.SetActive (false);
		}

		if (HeartRate >= 160) {
			PatientInfoText.text = "DEAD";
			KillPatient ();
		}




		//SKIN TONE
		if (GameObject.Find ("MasterController").GetComponentInChildren<MasterControl> ().Difficulty != 2) {
			
			//SKIN
			if (SkinColour < 0) {
				Arm.GetComponent<MeshRenderer> ().material.color = Color.blue;
				KillPatient ();

			}
			if ((SkinColour > 0) && (SkinColour <= 20)) {
				Arm.GetComponent<MeshRenderer> ().material.color = new Color (0.573f, 0.682f, 0.063f, 1);
				Invoke ("LowerSkintone", 0);
			}
			if ((SkinColour > 20) && (SkinColour <= 40)) {
				Arm.GetComponent<MeshRenderer> ().material.color = new Color (0.678f, 0.659f, 0.063f, 1);
				Invoke ("LowerSkintone", 0);
			}
			if ((SkinColour > 40) && (SkinColour <= 60)) {
				Arm.GetComponent<MeshRenderer> ().material.color = Color.white;	
				Invoke ("RaiseSkintone", 0);													
				GoodSkin = true;
			} else {
				GoodSkin = false;
			}
			if ((SkinColour > 60) && (SkinColour <= 80)) {
				Arm.GetComponent<MeshRenderer> ().material.color = new Color (0.761f, 0.384f, 0.282f, 1);
				Invoke ("RaiseSkintone", 0);
			}
			if ((SkinColour > 80) && (SkinColour <= 100)) {
				Arm.GetComponent<MeshRenderer> ().material.color = new Color (0.761f, 0.282f, 0.063f, 1);
				Invoke ("RaiseSkintone", 0);
			}
			if (SkinColour > 100) {
				Arm.GetComponent<MeshRenderer> ().material.color = Color.black;
				KillPatient ();
			}
		}

		//TEMPERATURE
		if (Temperature < 24) {
			PatientTemperature.text = "DEAD";
			KillPatient ();
		}
		if (Temperature >= 24 && Temperature <= 30) {
			Efficiency = 0;
			Panting.SetActive (true);
		}
		if (Temperature >= 31 && Temperature <= 35) {
			Efficiency = 1;
		}
		if (Temperature >= 36 && Temperature <= 37) {
			Efficiency = 2;
		}
		if (Temperature >= 38 && Temperature <= 40) {
			Efficiency = 1;
		}
		if (Temperature >= 41 && Temperature <= 44) {
			Efficiency = 0;
			Panting.SetActive (true);
		}
		if (Temperature > 44) {
			PatientTemperature.text = "DEAD";
			KillPatient ();
		}
		if (Temperature >= 30 && Temperature <= 41) {
			Panting.SetActive (false);
		}



		if (Temperature >= 24 && Temperature < 36.5f) {
			Temperature -= 0.01f * Time.deltaTime;				//-1 every 10 seconds

		}
		if (Temperature > 36.5f && Temperature <= 44) {
			Temperature += 0.01f * Time.deltaTime;				//+1 every 10 seconds
		}
	}


	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player") {
			PatientText.SetActive (true);
		}
	}
	void OnTriggerExit(Collider col){
		if (col.gameObject.tag == "Player") {
			PatientText.SetActive (false);
		}
	}

	void LowerLifeProgress(){
		LifeProgress -= 0.1f * Time.deltaTime + FasterSpeeds;
	}
	void RaiseLifeProgress100(){
		LifeProgress += 6 * Time.deltaTime - FasterSpeeds;
	}
	void RaiseLifeProgress75(){
		LifeProgress += 4 * Time.deltaTime - FasterSpeeds;
	}
	void RaiseLifeProgress50(){
		LifeProgress += 2 * Time.deltaTime - FasterSpeeds;
	}

	void LowerHeartRate(){
		HeartRate -= 0.05f * Time.deltaTime + FasterSpeeds;
		Coughing.SetActive (true);
	}

	void LowerHeartRateFast(){
		HeartRate -= 0.1f * Time.deltaTime + FasterSpeeds;
		Coughing.SetActive (false);
	}

	void RaiseHeartRate(){
		HeartRate += 0.05f * Time.deltaTime + FasterSpeeds;
		Coughing.SetActive (true);
	}

	void RaiseHeartRateFast(){
		HeartRate += 0.1f * Time.deltaTime + FasterSpeeds;
		Coughing.SetActive (false);
	}

	void LowerSkintone(){
		SkinColour -= 0.075f * Time.deltaTime + FasterSpeeds;
	}

	void RaiseSkintone(){
		SkinColour += 0.075f * Time.deltaTime + FasterSpeeds;
	}

	public void NewPatient(){
		GameObject.Find("GameController").GetComponentInChildren<GameControl>().PatientSaved += 1;
		GameObject.Find ("GameController").GetComponentInChildren<GameControl> ().RespawnPatient ();
		GameObject.Find ("GameController").GetComponentInChildren<GameControl> ().PlaySavedSound ();

	}

	public void KillPatient(){
		LifeProgress = 0;
		Destroy (Patient);
		GameObject.Find("GameController").GetComponentInChildren<GameControl>().PatientKilled += 1;
		GameObject.Find ("GameController").GetComponentInChildren<GameControl> ().RespawnPatient ();
		GameObject.Find ("GameController").GetComponentInChildren<GameControl> ().PlayDeathSound ();

	}

	public void LifeBar(){
		if (Efficiency == 2) {
			RaiseLifeProgress100 ();
		}
		if (Efficiency == 1) {
			RaiseLifeProgress75 ();
		}
		if (Efficiency == 0) {
			RaiseLifeProgress50 ();
		}
	}

}

