﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mixcolour : MonoBehaviour {

	public Material[] Mats;

	public float R, G, B;
	void Start () {

		R = ((GameObject.Find ("MixingBowl").GetComponentInChildren<Mixingcontrol> ().ColBaseR + GameObject.Find ("MixingBowl").GetComponentInChildren<Mixingcontrol> ().ColR) / 255);
		G = ((GameObject.Find ("MixingBowl").GetComponentInChildren<Mixingcontrol> ().ColBaseG + GameObject.Find ("MixingBowl").GetComponentInChildren<Mixingcontrol> ().ColG) / 255);
		B = ((GameObject.Find ("MixingBowl").GetComponentInChildren<Mixingcontrol> ().ColBaseB + GameObject.Find ("MixingBowl").GetComponentInChildren<Mixingcontrol> ().ColB) / 255);

		Mats = GetComponent<Renderer> ().materials;
		Mats [2].color = new Color (R, G, B, 1);
	}
	
	void Update () {
	}
}
